# Without parameters localizationPlugin will produce up to date localebuild into execution directory.
#
# -server flag, it will spawn webserver. Request will trigger localebuilder and respond with newest build, see ./config.json for settings.
#
# -writer flag will write new localization build to file after rebuild
#
# -upload flag not implemented yet. Its supposed to upload new build into S3 and update language version number at remote settings server.