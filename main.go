package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/atthfi95/lokalise-unity-localizationbuilder/internal"
)

var (
	locMap      map[string]map[string]string
	newContent  []string
	fileVersion string
	fallbacks   internal.Config
)

// Fetch bundle and parse it into unity locale csv format.
func main() {
	start := time.Now()
	flag.BoolVar(&internal.WriterFlag, "writer", false, "False by default. Set true if you want to save newest localebuild to file on each rebuild.")
	flag.BoolVar(&internal.ServerFlag, "server", false, "False by default. Set true to spawn httwebserver that replies requests with new locale build (csv format)")
	flag.BoolVar(&internal.UploaderFlag, "upload", false, "False by default. Set true to upload newContent into remotesettings server.")
	flag.Parse()

	// Hardcoded config.json fallback values. TODO: remove hardcode & dont print stuff when working correctly.
	fallbacks.Port = "9999"                                             // Fallback value
	fallbacks.Headers = "Key;Type;Desc;English;Finnish;Swedish;Spanish" // Fallback value
	fallbacks.ID = "REMOVED"                                            // Fallback value
	fallbacks.Server = "REMOVED"                                        // Fallback value
	fallbacks.Directory = "./"                                          // Fallback value
	fallbacks.Tokens = append(fallbacks.Tokens, internal.Token{
		APIToken: "REMOVED", // Fallback value
		Owner:    "REMOVED",
	})
	fallbacks.ACL = "private"
	fallbacks.BucketRegion = "eu-west-1"
	fallbacks.BucketName = "REMOVED"

	// Read config.json
	c, err := internal.ReadConfig("./config.json")
	if err != nil {
		fmt.Printf("Loading config failed. %v.\nUsing fallback values!\n", err) // If config.json not found
		c = fallbacks                                                           // Using fallback values.
	}

	// Fetch bundle url from lokalise.co
	locMap, err := internal.FetchBundleURL(c.ID, c.Tokens)
	if err != nil {
		fmt.Printf("Error! %v", err)
	}

	// Fetch localization build version from remote settings server.
	fileVersion, err := internal.FetchVersion(c.Server)
	if err != nil {
		fmt.Printf("Error! %v\nUsing fallback filename: %s", err, fileVersion)
		fileVersion = "localebuild" + internal.Timestamp() + "" // Fallback value
	}

	// Parse new localebuild.
	newContent = internal.BuildCSVFormat(c.Headers, locMap)

	// Write build to file on disk.
	if internal.WriterFlag {
		fmt.Println("Writing new localebuild...")
		if err := internal.WriteToFile(newContent, c.Directory, fileVersion); err != nil {
			fmt.Printf("Writing failed!!\n%v\n", err)
		}
		if internal.ServerFlag {
			go internal.ToRoutine(15, fallbacks)
		}
	}

	// Upload build to S3.
	if internal.UploaderFlag { // Uploads file to S3, false by default.
		fmt.Println("Uploading locale build into S3...")
		if err := internal.UploadToRemote(c, newContent, fileVersion); err != nil {
			fmt.Printf("Uploading failed!\n%v\n", err)
		}
	}

	fmt.Printf("\nNew localebuild done in %v!\n", time.Since(start))
	if internal.ServerFlag { // Not executed by default.
		fmt.Println("Starting server mode...")
		if err := internal.ServeToClient(c.Port, newContent, fileVersion, fallbacks); err != nil {
			fmt.Printf("Could not launch in server mode!\n%v\n", err)
		}
	}
}
