package internal

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
)

// DownloadResponse is json response of lokalise.co API, it contains bundle url used to download bundled translations.
type DownloadResponse struct {
	ID  string `json:"project_id"`
	URL string `json:"bundle_url"`
}
type settingsObject struct {
	ID        string
	Key       string
	Value     string
	Privilege string
}
type remoteSettings struct {
	Success bool
	Data    []settingsObject
}

// FetchVersion requests current language build version from remote server, version is used in filenaming.
func FetchVersion(server string) (string, error) {
	start := time.Now()
	println("Fetching remote settings version...")

	if server == "" {
		server = "REMOVED"
	}

	var target remoteSettings

	endpoint := server + "/u/1/remotesettings/getall"
	res, err := http.Get(endpoint)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	json.NewDecoder(res.Body).Decode(&target)
	for _, obj := range target.Data {
		if obj.Key == "language_version" {
			defer fmt.Printf("Fetched version %v in %v \n", obj.Value, time.Since(start))
			return obj.Value, nil
		}
	}
	return "", fmt.Errorf("couldnt fetch locale version from %v", server)
}

// FetchBundleURL loads bundle url from lokalise.co API and passes it to FetchLocale, returning map of maps.
func FetchBundleURL(projectID string, apiTokenList []Token) (map[string]map[string]string, error) {
	start := time.Now()
	println("Downloading bundle url...")

	var err error
	var locMap map[string]map[string]string

	for attempts := 0; attempts < len(apiTokenList); attempts++ { // Iterate through api tokens.
		apiToken := apiTokenList[attempts]
		if attempts != 0 {
			println("\nAttempting fetch with different apiToken!\n")
		}
		locMap, err = FetchLocale(projectID, apiToken.APIToken) // Attempt request with current api token.
		if err != nil {
			println("Fetch failed!")
			time.Sleep(1 * time.Second)
		} else {
			break // Break out when valid token is found!
		}
	}
	if err != nil {
		return nil, fmt.Errorf("no valid apiToken in preset, cannot fetch")
	}

	defer fmt.Printf("Downloading finished in %v \n", time.Since(start))
	return locMap, nil
}

// FetchLocale requests translation bundle from lokalise api & returns true if fetch and unzipped succesfully.
func FetchLocale(projectID string, apiToken string) (map[string]map[string]string, error) {
	url := "https://api.lokalise.co/api2/projects/" + projectID + "/files/download"
	// Raw req json body
	raw := strings.NewReader(`{
		"format": "json",
		"original_filenames": false,
		"export_empty_as": "base",
		"all_platforms": true
	}`)

	req, err := http.NewRequest("POST", url, raw)
	if err != nil {
		return nil, fmt.Errorf("request error: %v", err)
	}
	req.Header.Add("x-api-token", apiToken) // Add user specific api-token.

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed request to url: "+url+"\n %v", err)
	}
	defer res.Body.Close()

	var data DownloadResponse
	if json.NewDecoder(res.Body).Decode(&data) != nil {
		return nil, fmt.Errorf("failed to parse json response")
	}

	bres, err := http.Get(data.URL)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch bundle! %v", err)
	}

	bbody, err := ioutil.ReadAll(bres.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading bundled response %v", err)
	}
	zipr, err := zip.NewReader(bytes.NewReader(bbody), int64(len(bbody)))
	if err != nil {
		return nil, err
	}

	locMap := make(map[string]map[string]string)
	for _, f := range zipr.File {
		if filepath.Ext(f.Name) != ".json" {
			continue
		}
		rc, err := f.Open()
		if err != nil {
			return nil, err
		}
		var loc map[string]string
		if err := json.NewDecoder(rc).Decode(&loc); err != nil {
			rc.Close()
			return nil, err
		}
		rc.Close()
		locMap[filepath.Base(strings.TrimSuffix(f.Name, ".json"))] = loc
	}
	return locMap, nil
}

// ServeToClient Starts webserver for builder. Returns new localebuild.
func ServeToClient(port string, data []string, fileVersion string, fallbacks Config) error {
	println("Server mode up and running!")

	router := httprouter.New()
	router.GET("/", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		start := time.Now()

		w.Header().Set("Content-Type", "text/csv; charset=utf-8")
		w.Header().Set("Content-Disposition", "attachment; filename="+fileVersion+".txt")

		if WriterFlag { // If Writing is turned on, use existing file.
			serve, _ := ReadFromFile("./" + fileVersion + ".txt")
			w.Write(serve) // Serve
		} else { // Else serve new build on every request.
			var c Config
			c, err := ReadConfig("/root/localizationPlugin/config.json") // Default dir hardcode.
			if err != nil {
				c, err = ReadConfig("./config.json") // Current dir.
				if err != nil {
					c, err = ReadConfig("~/localizationPlugin/config.json") // Default dir at different users.
				}
			}
			if err != nil {
				println(fmt.Sprintf("Loading config failed. %v.\nUsing fallback values!\n", err)) // If config.json not found
				c = fallbacks
			}
			locMap, _ := FetchBundleURL(c.ID, c.Tokens)
			fileVersion, _ = FetchVersion(c.Server)
			newContent := BuildCSVFormat(c.Headers, locMap)
			writeTo(w, newContent) // Serve
		}
		fmt.Printf("Current localebuild served in %v \n", time.Since(start))
	})
	if err := http.ListenAndServe("0.0.0.0:"+port, router); err != nil {
		return fmt.Errorf("server crashed %v", err)
	}
	return nil
}
