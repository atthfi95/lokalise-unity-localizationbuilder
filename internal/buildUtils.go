package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var (
	// ServerFlag when turned true, will spawn webserver which responses with new locale build.
	ServerFlag bool
	// WriterFlag when true, will PREVENT writing new locale build on disk.
	WriterFlag bool
	// UploaderFlag when true, will upload new localebuild to S3 and update remote settings language version.
	UploaderFlag bool
	// WriterLock locks file when writing, unlock when done. Always check lock before reading file.
	writerLock sync.RWMutex
)

// Config is type for necessary values stored in config.json.
type Config struct {
	Port         string  `json:"buildServerPort"`
	Headers      string  `json:"csvHeaders"`
	ID           string  `json:"projectID"`
	Server       string  `json:"remoteServer"`
	Directory    string  `json:"localOutputDirectory"`
	Tokens       []Token `json:"tokens"`
	ACL          string  `json:"acl"`
	BucketRegion string  `json:"bucketRegion"`
	BucketName   string  `json:"bucketName"`
}

// Token is type for lokalise.co api response.
type Token struct {
	APIToken string `json:"apiToken"`
	Owner    string `json:"owner"`
}

// S3Bundle carries data to create aws connection and payload which will be uploaded to amazon S3 storage
type S3Bundle struct {
	NewLocalizationBuild []string
	S3Region             string
	S3Bucket             string
	ACL                  string
	Fileversion          string
}

func addFileToS3(awssession *session.Session, version string, c Config) error {
	// Attempt to open localebuild from disk, if any.
	fileDir := "./" + version + ".txt"
	file, err := os.Open(fileDir)
	if err != nil { // If no file can be reached, use translation []string.
		return fmt.Errorf("unable to read new build.\nupload failed %v", err)
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return fmt.Errorf("file info reading failed: %v", err)
	}
	size := fileInfo.Size()
	buffer := make([]byte, size)
	file.Read(buffer)

	println("Uploading to S3...")
	if _, err = s3.New(awssession).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(c.BucketName),
		Key:                  aws.String(fileDir[2:]),
		ACL:                  aws.String(c.ACL),
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(size),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	}); err != nil {
		return fmt.Errorf("upload failed: %v", err)
	}
	return err
}

// UploadToRemote NOT DONE YET! uploads complete locale build into remote settings server.
func UploadToRemote(c Config, build []string, version string) error {
	awssession, err := session.NewSession(&aws.Config{Region: aws.String(c.BucketRegion)})
	if err != nil {
		return fmt.Errorf("failed to create aws session %v", err)
	}

	err = addFileToS3(awssession, version, c)
	if err != nil {
		return fmt.Errorf("failed to upload file to aws s3 %v", err)
	}

	return err
}

// ReadConfig reads config.json from exec dir, returning projectID and api tokens from it.
func ReadConfig(filepath string) (Config, error) {
	start := time.Now()
	var c Config
	println("Reading config...")

	configFile, err := os.Open(filepath)
	if err != nil {
		return Config{}, fmt.Errorf("loading config failed. %v.\nUsing fallback values", err)
	}
	defer configFile.Close()
	defer fmt.Printf("Reading done in %v \n", time.Since(start))

	if json.NewDecoder(configFile).Decode(&c) != nil {
		return Config{}, fmt.Errorf("failed to parse config file")
	}
	return c, nil
}

// WriteToFile Writes csv formated string list into a file.
func WriteToFile(data []string, path string, fileVersion string) error {
	writerLock.Lock()
	defer writerLock.Unlock()

	start := time.Now()
	println("Output path set to ", path)
	println("Filename set to ", fileVersion+".txt")
	println("Writing " + strconv.Itoa(len(data)+1) + " lines to file " + fileVersion + ".txt...")

	file, err := os.Create(path + fileVersion + ".txt")
	if err != nil {
		println("Unable to write into set directory. Using execution directory...")
		file, err = os.Create(fileVersion + ".txt")
		if err != nil {
			return err
		}
	}
	defer file.Close()

	for i, line := range data {
		if i == 0 {
			_, err = io.WriteString(file, line)
		} else {
			_, err = io.WriteString(file, "\n"+line)
		}
		if err != nil {
			return err
		}
	}
	fmt.Printf("Writing done in %v \n", time.Since(start))
	return file.Sync()
}

// ReadFromFile reads file from given path when writerLock allows.
func ReadFromFile(path string) ([]byte, error) {
	writerLock.RLock()
	defer writerLock.RUnlock()
	return ioutil.ReadFile(path)
}

// BuildCSVFormat formats map of map of strings into unity csv locale format and returns that slice of strings.
func BuildCSVFormat(csvHeaders string, locMap map[string]map[string]string) []string {
	start := time.Now()
	fmt.Println("Building locales...")

	var output []string
	output = append(output, csvHeaders)
	for l, loc := range locMap["en"] {
		line := l + ";Text;;\"" + loc + "\";\"" + locMap["fi"][l] + "\";\"" + locMap["sv"][l] + "\";\"" + locMap["es"][l] + "\""
		out := fmt.Sprint(strings.ReplaceAll(fmt.Sprint(strings.ReplaceAll(line, "\\t", "\t")), "\\n", "\n"))
		output = append(output, out)
	}

	fmt.Printf("Building done in %v\n", time.Since(start))
	return output
}

// ToRoutine will execute lcoale rebuild frequently based on given value in minutes.
func ToRoutine(frequency int, fallbacks Config) {
	if frequency < 3 {
		println("Rebuild frequency below minimum, set to 3.")
		frequency = 3
	}
	println(fmt.Sprintf("\nNew build will be written on disk every %v minute...\n", frequency))

	run := true
	for run {
		time.Sleep(time.Duration(frequency) * time.Minute)
		println("Routine rebuild triggered...")
		c, err := ReadConfig("/root/localizationPlugin/config.json")
		if err != nil {
			c, err = ReadConfig("./config.json")
			println(fmt.Sprintf("Loading config failed. %v.\nUsing fallback values!\n", err)) // If config.json not found
			c = fallbacks
		}

		locMap, _ := FetchBundleURL(c.ID, c.Tokens)
		fileVersion, _ := FetchVersion(c.Server)
		newContent := BuildCSVFormat(c.Headers, locMap)
		if WriteToFile(newContent, c.Directory, fileVersion) != nil {
			println("Routine building failed!")
		} else {
			UploadToRemote(c, newContent, fileVersion)
			println("Routine build success!\n")
		}
	}
}

// Timestamp returns current date and time in format YYYYMMDD-HHMMSS.
func Timestamp() string {
	t := time.Now()
	return fmt.Sprintf("%d%02d%02d-%02d%02d%02d",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())
}

// prepend adds line at the start of a string slice and returns result.
func prepend(self []string, insert string) []string {
	var output []string
	output = append(output, insert)
	for _, line := range self {
		output = append(output, line)
	}
	return output
}

// toBytes converts string slice into slice of bytes
func toBytes(input []string) []byte {
	buffer := &bytes.Buffer{}
	for _, line := range input {
		buffer.WriteString(line)
		buffer.WriteRune('\n')
	}
	return buffer.Bytes()
}

func writeTo(output io.Writer, input []string) {
	for _, line := range input {
		output.Write([]byte(line))
		output.Write([]byte{'\n'})
	}
}
